## Localbitcoins Trader

### About

Localbitcoins trader is an application that helps with trading on Localbitcoins.

It is built as a thin web application and distributed as a single jar file. It is expected to be run locally or on a private virtual server.

### Benefits
 - Gives you an overview of your own and your competitors' and the markets' price movements over the day and in the past, and makes it easier to understand their strategies.
 - Allows you to automatically keep your prices within a range, for example 1% below your competitors' prices but never less than 3% over a market price such as bitstamp or kraken or a mixture of markets.
 - Allows you to automatically keep the minumum amount for a trade dependent on the current price so that a minimum revenue is reached for each transaction.
 - *allows you to define your competition in a flexible way in terms of currency, payment method, volume, etc.*
 - *receive an SMS alert for events such as sudden market movements.*
 - Open source available on gitlab.com for all cooperating partners, but not redistributable.

Items in italic are under construction

### Installing

#### Requirements

 - Linux, Macintosh or Windows operating system.
 - Java JDK 8 or higher
 - Localbitcoins API credentials (goto `https://localbitcoins.com/accounts/api/`)
 - Optional: A sendclick API key if you want to receive SMS warnings on certain events.

The application is distributed as

 - a single jar file,
 - an inital configuration file and
 - a shell script or a window bat file to run it.

Copy the jar file and the shell script to a subdirectory under you home directory. For example `trader`.
Copy the config file to your home directory. It should be called `.trader.config.json.` Notice the inital dot.

Edit the config to add the Localbitcoins credentials and optionally the SendClick credentials.

### Running

On windows open a terminal window and run the bat file. Leave the terminal open as long as you want the application to run.

On linux, run the application in a terminal. Leave the terminal open as long as you want the application to run.

You can now access the application using your web browser by navigating to `http://localhost:5050/config`

### Using

#### Chart

![Chart](doc/images/chart.png)

The chart page show the price history for you ads and you competitor's ads for 1 day. Hovering over a priceline will display a short discription of the ad, and links to the localbitcoin pages for the owner and the ad it self. For you own ads, a pencil icon links to the edit page for you ad.

Since it is the application itself that collects these information, it will only show prices for times it has been running.

#### Config

![Chart](doc/images/config.png)

The configuration page allows you to control various aspects of your application and individual ads.

#### Log

The log shows you, minute by minute the actions taken and the reason for price changes made.

